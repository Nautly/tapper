//
//  ViewController.swift
//  tapper
//
//  Created by Andrew Wilson on 11/17/15.
//  Copyright © 2015 Andrew Wilson. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    //Properties
    var maxTaps:Int = 0
    var currentTaps: Int = 0
    
    //Outlets
    @IBOutlet weak var playBtn: UIButton!
    @IBOutlet weak var howManyTapsTxt: UITextField!
    @IBOutlet weak var logoImg: UIImageView!
    
    @IBOutlet weak var tapBtn: UIButton!
    @IBOutlet weak var tapCountLbl: UILabel!
    
    //Actions
    @IBAction func onTabButtonPressed(sender: UIButton) {
        currentTaps++
        updateTapsLbl()
        
        if isGameOver() {
           restartGame()
        }
    }
    
    @IBAction func onTapButtonPressed() {
        
        if howManyTapsTxt.text != nil && howManyTapsTxt.text != "" {
            
            playBtn.hidden = true
            howManyTapsTxt.hidden = true
            logoImg.hidden = true
            
            tapBtn.hidden = false
            tapCountLbl.hidden = false
            
            maxTaps = Int(howManyTapsTxt.text!)!
            currentTaps = 0
            
            updateTapsLbl()
        }
    }
    
    func restartGame() {
        maxTaps = 0
        howManyTapsTxt.text = ""
        playBtn.hidden = false
        howManyTapsTxt.hidden = false
        logoImg.hidden = false
        
        tapBtn.hidden = true
        tapCountLbl.hidden = true
    }

    func isGameOver() -> Bool {
        if currentTaps >= maxTaps {
            return true
        }
        
        return false
    }
    
    func updateTapsLbl() {
        tapCountLbl.text = "\(currentTaps) Taps"
    }

}

